const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const passport = require('./src/config/passport')
const Routes = require('@/routes');

const app = express();
const PORT = process.env.PORT || 4000

app.use(cors() )

app.listen(PORT, ()=>{
  console.log(`Server listening to port ${PORT}...`);
})

app.use(bodyParser.json())

app.use('/api', Routes);
