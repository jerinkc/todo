const Sequelize  = require('sequelize');

const connection = new Sequelize('postgres', 'postgres', 'root', {
  host: 'localhost',
  dialect: 'postgres'
});

connection.authenticate().then(console.log('Database Authenticated'))
  .catch(error=> console.log(error))

module.exports = connection ;


