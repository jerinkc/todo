const { DataTypes } = require('sequelize');

const connection = require('@/sequelize/config');
const Task = require('@/sequelize/models/task');

const User = connection.define('User', {
  email:{
    type: DataTypes.STRING
  },
  password:{
    type: DataTypes.STRING
  }
});

User.hasMany( Task );

connection.sync( { alter: true } ).then( console.log('Altered table - Users') )
  .catch(error=> console.log(error))

module.exports = User;


