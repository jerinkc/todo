const { DataTypes } = require('sequelize');

const connection = require('@/sequelize/config');
const user = require('@/sequelize/models/user')

const Task = connection.define('Task', {
  title:{
    type: DataTypes.STRING,
    allowNull: false
  },
  content: {
    type: DataTypes.STRING
  },
  shared:{
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  completed:{
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
})

connection.sync( { alter: true } ).then(console.log('Altered table - tasks'))
  .catch(error => console.log(error))

module.exports = Task;
