const passport = require('passport');

const User = require('@/sequelize/models/user');

function authenticate(){
  passport.authenticate('google', {
    scope: ['profile']
  })
}

function index( req, res ){
  //lists all users
  // Task.findAll().then(tasks => res.json(tasks))
}

function show( req, res ){
  //describe details of corresponding users
  // const { id } = req.params

  // Task.findOne({ where: { id: id }}).then(task=> res.json(task))
  //   .catch(error=> res.send(error))
}

function create( req, res ){
  //create one user
  // const { task } = req.body

  // Task.create(task).then(savedTask => res.json(savedTask))
  //   .catch(error => res.send(error))
}

function update( req, res ){
  // update name and etc,
  // const { id } = req.params

  // Task.update(req.body.task, { returning: true, plain: true, where: {id: id} } )
  //   .then(updatedTask =>{
  //     console.log(updatedTask[1].dataValues)
  //     const { dataValues } = updatedTask[1] ;
  //     res.json( dataValues )
  //   })
  //   .catch(error => res.send(error))
}

function destroy( req, res ){
  // removes the user
  // const { id } = req.params

  // Task.destroy( { where: { id: id } } ).then(task=> res.json(task))
  //     .catch(error=> res.send(error))
}

module.exports = { index, show, create, update, destroy, authenticate }
