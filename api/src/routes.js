const express = require('express');
const passport =  require('passport')

const Tasks = require('@/controllers/tasks');
const users = require('@/controllers/users');

const router = express.Router();

router.get('/login', passport.authenticate('google', { scope: ['profile']}));
router.get('/tasks/', (req, res) => Tasks.index(req, res));
router.post('/tasks/', (req, res) => Tasks.create(req, res));
router.get('/tasks/:id', (req, res) => Tasks.show(req, res));
router.put('/tasks/:id', (req, res) => Tasks.update(req, res));
router.delete('/tasks/:id', (req, res) => Tasks.destroy(req, res));

module.exports = router;

