import React from 'react';
import { Switch, Route, Redirect }  from 'react-router-dom';

import TaskListing from './components/task/Listing'
import Login from './components/Login';

export default function(){
  return(
    <Switch>
      <Route exact path="/">
        <Redirect to="/login"/>
      </Route>
      <Route exact path="/login" component={ Login }/>
      <Route exact path="/tasks" component={ TaskListing } />
    </Switch>
  )
}

